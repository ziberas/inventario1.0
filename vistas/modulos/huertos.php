
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Huertos
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar Huertos</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCategoria">
          
          Agregar Huerto

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>Propietario</th>
           <th>Ubicacion</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>
